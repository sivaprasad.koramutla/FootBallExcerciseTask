﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FootBallExercise;

namespace IntegrationTest
{
    [TestClass]
    public class CSVOpearationTest
    {
        [TestMethod]
        public void displaySmalldiffresult()
        {
            ICSVReader _csvopearation = new CSVReader();
          var result= _csvopearation.displaySmallDiffTeam(@Environment.CurrentDirectory + "\\football.csv");
            Assert.AreEqual("Team with smallest goal diffrence is  8. Aston_Villa", result);
        }
    }
}
