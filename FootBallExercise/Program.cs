﻿using System;
using System.Configuration;

namespace FootBallExercise
{
    public class Program
    {
        private const string message = "Please enter the csv file path  \n\n";
        public static void Main(string[] args)
        {
            ICSVReader _csvopearation = new CSVReader();
            string displayedresult = "";
            try
            {
                Console.Write(message);
                Console.Out.Flush();
                var inputFilePath = Console.ReadLine();


               
                displayedresult = _csvopearation.displaySmallDiffTeam(inputFilePath);
                Console.Write(displayedresult);
                Console.WriteLine("\n\n\nPress any key to exit");
                Console.ReadKey(true);
            }

            catch (Exception ex)
            {
                Console.WriteLine("\n\nFile not found.");
                Console.WriteLine("\n\n\nPress any key to exit");
                Console.ReadKey(true);
            }
        }

    }
}



