﻿using System.Collections.Generic;

namespace FootBallExercise
{
    public interface IFileValidations
    {
        //Interface for CSV FileValidations related functions
        bool HeadersValidate(List<string> columns);
        bool validateFileFormat(string filepath);
        TournmentData CSVDataParse(string csvLine);
    }
}