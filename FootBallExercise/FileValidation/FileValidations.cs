﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootBallExercise
{
    public class FileValidations : IFileValidations
    {
        private const string appSetKey_headerFormat = "headerformat";
        public string headerColumns = ConfigurationSettings.AppSettings[appSetKey_headerFormat];
        private string allowedFormat = ".csv";

        /// <summary>
        /// validateFileFormat method validates the  file format and return bool value.
        /// Return type is bool,If file format is validated  returns the true else it returns false
        /// </summary>
        /// <param name="filepath">a string, exist physical CSV file path,
        /// that contains all the English Premier League data>/param> 
        public bool validateFileFormat(string filepath)
        {
            bool validFile = false;
            string extension = Path.GetExtension(filepath);
            if (extension == allowedFormat)
            {
                validFile = true;
                return validFile;
            }
            return validFile;
        }

        /// <summary>
        /// Function HeadersValidate validates the CSV file header format and return boolean value.
        /// Return type is boolean ,If header is validated it returns the true else it returns false
        /// </summary>
        public bool HeadersValidate(List<string> columns)
        {
            try
            {
                int i = 0;
                foreach (string val in columns)
                {
                    if (!headerColumns.Split(',').ToList<string>()[i].Equals(val))
                    {
                        return false;
                    }
                    i++;
                }
            }
            catch (Exception)
            {

                throw new AggregateException(string.Format("Error occured in forming colummn header Row: It should be '{0}'. ", headerColumns));
            }
            return true;
        }

        /// <summary>
        /// Function to parse the  data and populate the list of Tournmentteams Model data 
        ///All the Premier league data parsed is added to the Tournment List
        /// Returns list of Teams data.
        /// </summary>

        public TournmentData CSVDataParse(string csvLine)
        {
            TournmentData tournment = new TournmentData();
            try
            {
                csvLine.Trim();
                string[] values = csvLine.Split(',');
                {
                    /*Checking if CSV data file row contains delimiter skipping row*/
                    if (!csvLine.Contains("--"))
                    {
                        tournment.Team = Convert.ToString(values[0]);
                        tournment.P = Convert.ToInt32(values[1]);
                        tournment.W = Convert.ToInt32(values[2]);
                        tournment.L = Convert.ToInt32(values[3]);
                        tournment.D = Convert.ToInt32(values[4]);
                        tournment.F = Convert.ToInt32(values[5]);
                        tournment.A = Convert.ToInt32(values[7]);
                        tournment.Pts = Convert.ToInt32(values[8]);
                    }
                }

            }
            catch
            {
                throw new ArgumentException(string.Format("Error occured in forming Data Row: '{0}'", Convert.ToString(csvLine)));
            }
            return tournment;
        }
    }
}
