﻿using System.Collections.Generic;

namespace FootBallExercise
{
    public interface ICSVReader
    {
        //Interface for CSVReader related operation
        List<TournmentData> ParseCSVData(string filepath);
        string displaySmallDiffTeam(string filepath);
        bool validateCSVfileheader(string filepath);

    }
}