﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace FootBallExercise
{
    public class CSVReader : ICSVReader
    {
        /// <summary>
        /// validateCSVFileheader method validates the CSV file header format and return boolean value.
        /// Return type is boolean,If header is validated it returns the true else it returns false
        /// </summary>
        /// <param name="filepath">a string, exist physical CSV file path,
        /// that contains all the English Premier League data>/param> 


        IFileValidations _validate = new FileValidations();
        public bool validateCSVfileheader(string filepath)
        {
            bool headersvalidated = false;
          
            
            try
            {
                string headStr = File.ReadAllLines(filepath).FirstOrDefault();
                headersvalidated = _validate.HeadersValidate(headStr.Split(',').ToList<string>());
            }
            /* Here Catching the exception and returning proper message to Console window*/
            catch (Exception ex)
            {
                throw new AggregateException("Error : File " + filepath + " either does not exist or is empty");
            }
            return headersvalidated;

        }


        /// <summary>
        /// displaySmallDiffTeam method calls 'ParseCSVData' method internally and get total CSV file rows.
        ///After getting list of EPL teams records, calling Teamgoal diffrence and  returning small diffrence team name.
        /// </summary>
        /// <param name="filepath">a string, exist physical CSV file path,
        /// that contains all the English Premier League records>/param> 
        public string displaySmallDiffTeam(string filepath)
        {
            string returnoutput;
           
            try
            {
                List<TournmentData> listtournmentdata = ParseCSVData(filepath).ToList();
                listtournmentdata.Remove(listtournmentdata.Single(x => x.Team == null));
                TournmentData t = listtournmentdata.Select(x => x).OrderBy(x => x.goaldifference).FirstOrDefault();
                returnoutput = "Team with smallest goal diffrence is  " + t.Team;
                return returnoutput;
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }


        /// <summary>
        /// Function to parse the  data and populate the list of Tournmentteams Model data 
        ///All the Premier league data parsed is added to the Tournment List
        /// Returns list of Teams data.
        /// </summary>
        public List<TournmentData> ParseCSVData(string filepath)
        {
            bool isheaderValidated = false;
            try
            {

                List<TournmentData> teamsList = new List<TournmentData>();
                isheaderValidated = validateCSVfileheader(filepath);
                if (isheaderValidated)
                {
                   /*
                   In CSVfile, first row we are getting header column names .The file header validation is already done.
                   Skipping the first row of headers and parsing  data.
                   */
                    {
                        teamsList = File.ReadAllLines(filepath)
                                                      .Skip(1)
                                                      .Select(v => _validate.CSVDataParse(v))
                                                       .ToList();
                      
                    }
                }
                return teamsList;
            }

                  /*
                  If Parsing of CSV data rows encounter with any exception,  catching exception in CSVDataParse method
                  and throwing the exception with  message to console screen.
                  */
            catch (Exception ex)
            {
                throw new AggregateException(ex.Message);
            }
        }

    }
}
