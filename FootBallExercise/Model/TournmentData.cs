﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootBallExercise
{
    public class TournmentData
    {
        private int _goaldifference;
        public string Team { get; set; }
        public int P { get; set; }
        public int W { get; set; }
        public int L { get; set; }
        public int D { get; set; }
        public int F { get; set; }
        public int A { get; set; }
        public int Pts { get; set; }
        /*
         *To find out the team with the smallest goal difference, using Math absolute value of Substrating Goalsfor and goalsagainist values.
         * and Returns the goal diffrence value.
         */
        public int goaldifference
        {
            get
            {
                return Math.Abs(F - A);
            }
        }
    }
}
