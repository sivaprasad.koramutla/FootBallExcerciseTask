﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rhino.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using FootBallExercise;

namespace FootBallExerciseTest.FileValidationTest
{
    [TestClass]
    public class FileValidationTest
    {
        [TestMethod]
        public void validateFileFormat_method_should_return_True_when_we_pass_allowedforamt_file()
        {
            //Arrange
            string filepath = "test.csv";
            IFileValidations filevalidation = new FileValidations();
            //act
            var result = filevalidation.validateFileFormat(filepath);

            //assert
            Assert.AreEqual(true,result);
         
          
        }

        public void validateFileFormat_method_should_return_false_when_we_pass_allowedforamt_file()
        {
            //Arrange
            string filepath = "test.txt";
            IFileValidations filevalidation = new FileValidations();
            //act
            var result = filevalidation.validateFileFormat(filepath);

            //assert
            Assert.AreEqual(false, result);


        }
        [TestMethod]
        public void HeadersValidate__method_should_return_true_when_we_pass_allowedforamt_file()
        {
            string header ="Team,P,W,L,D,F,-,A,Pts";
            IFileValidations filevalidation = new FileValidations();
            //act
            var result = filevalidation.HeadersValidate(header.Split(',').ToList<string>());

            //assert
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void HeadersValidate__method_should_return_false_when_we_pass_allowedforamt_file()
        {
            string header = "Team,P,X,L,D,F,-,A,Pts";
            IFileValidations filevalidation = new FileValidations();
            //act
            var result = filevalidation.HeadersValidate(header.Split(',').ToList<string>());

            //assert
            Assert.AreEqual(false, result);
        }

    }
}
