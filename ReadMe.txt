Solution

--------


Implemented the solution as .NET console applicaiton.

Code hierarchy
------------------

CSVReader opearations:
----------------------
CSVReader class handles the business opeations like reading CSV files and valling Validation function.
ICSVReader.cs
CSVReader.cs

File validations
--------------------
Filevalidation class handles the validation related opearations like header validation,File format validations.

IFileValidations.cs
FileValidations.cs

Model
-----
This solution consists model file.
TournmentData.cs

Main Class
----------
Program.cs is main class


Assumption
----------
1. Application works with the data format as presnet in footbal.csv file
2. Application expects the input of the csv file path in Console window.

3. Validated rows with delimeter, if cells consists multiple delemiters,that rows is skipped.
4. When calculating the goal difference absolute value is used for comparison
   i.e. GoalsFor - GoalsAgainst is done and then absolute value of this is taken

Instruction to Run
------------------
1. Open the program in visual studio and run the program
2. Enter the location path of CSV file in console window